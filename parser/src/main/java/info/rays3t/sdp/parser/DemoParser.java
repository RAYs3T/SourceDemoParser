package info.rays3t.sdp.parser;


import info.rays3t.sdp.BaseValveFormatParser;
import info.rays3t.sdp.InvalidValveFileException;


import java.io.File;
import java.io.IOException;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 * Source and OrangeBox demos header reader.
 * This is a Java variant from the original by PoLaRiTy (nocheatz.com) postet at https://developer.valvesoftware.com/wiki/DEM_Format
 * <p>
 * https://developer.valvesoftware.com/wiki/DEM_Format
 * Help from   : https://developer.valvesoftware.com/wiki/DEM_Format#Demo_Header
 * http://hg.alliedmods.net/hl2sdks/hl2sdk-css/file/1901d5b74430/public/demofile/demoformat.h
 * <p>
 * Types sizes :
 * Int : 4 bytes
 * Float : 4 bytes
 * String : 260 bytes
 */
public class DemoParser extends BaseValveFormatParser {

    public static final int LENGTH_STRING = 260;
    public static final int LENGTH_INT = 4;
    public static final int LENGTH_FLOAT = 4;
    public static final int LENGTH_DEMO_TYPE = 8;
    public static final String EXPECTED_TYPE = "HL2DEMO";

    private DemoInfo demo;

    public DemoParser(File demoFile) throws InvalidValveFileException, IOException {
        super(demoFile);
    }

    /**
     * Reads the demo type and check if it is valid / supported.
     *
     * @throws IOException               if the stream read fails
     * @throws InvalidValveFileException if the type is invalid / unsupported
     */
    protected void readAndCheckType() throws IOException, InvalidValveFileException {
        String type = readString(LENGTH_DEMO_TYPE);
        if (!EXPECTED_TYPE.equals(type)) {
            throw new InvalidValveFileException("The given file is not a valid " + EXPECTED_TYPE + " file.");
        }
    }

    protected void parseFields() throws IOException {
        demo = new DemoInfo();
        demo.setDemoProtocol(readInt(LENGTH_INT));
        demo.setNetProtocol(readInt(LENGTH_INT));
        demo.setHostName(readString(LENGTH_STRING));
        demo.setClientName(readString(LENGTH_STRING));
        demo.setMapName(readString(LENGTH_STRING));
        demo.setGameDir(readString(LENGTH_STRING));
        demo.setTime(readFloat(LENGTH_FLOAT));
        demo.setTicks(readInt(LENGTH_INT));
        demo.setFrames(readInt(LENGTH_INT));
        demo.setType(demo.isGoodIPPORTFormat() ? (byte) 0 : (byte) 1);
    }

    public DemoInfo getDemo() {
        return demo;
    }

}
