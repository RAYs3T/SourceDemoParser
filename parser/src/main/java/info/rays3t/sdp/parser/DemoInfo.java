package info.rays3t.sdp.parser;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 */
public class DemoInfo {
    private int demoProtocol;

    public int getDemoProtocol() {
        return demoProtocol;
    }

    public void setDemoProtocol(int demoProtocol) {
        this.demoProtocol = demoProtocol;
    }

    private int netProtocol;

    public int getNetProtocol() {
        return netProtocol;
    }

    public void setNetProtocol(int netProtocol) {
        this.netProtocol = netProtocol;
    }

    private String hostName;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    private String clientName;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    private String mapName;

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    private String gameDir;

    public String getGameDir() {
        return gameDir;
    }

    public void setGameDir(String gameDir) {
        this.gameDir = gameDir;
    }

    private float time;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    private int ticks;

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    private int frames;

    public int getFrames() {
        return frames;
    }

    public void setFrames(int frames) {
        this.frames = frames;
    }

    public int getTickrate() {
        return Float.valueOf(getTicks() /  getTime()).intValue();
    }

    // 0 = RIE, 1 = TV
    private byte type;

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    private boolean statusPresent;

    public boolean isStatusPresent() {
        return statusPresent;
    }

    public void setStatusPresent(boolean statusPresent) {
        this.statusPresent = statusPresent;
    }

    public boolean isGoodIPPORTFormat() {
        if(getHostName() == null){
            return false;
        }
        return getHostName().matches("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\:[0-9]{1,5}");
    }
}
