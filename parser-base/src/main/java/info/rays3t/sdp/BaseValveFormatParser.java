package info.rays3t.sdp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by RAYs3T-BWS on 13.04.2017.
 */
public abstract class BaseValveFormatParser {
    private InputStream is;
    private int cursor = 0;

    private BaseValveFormatParser() {
        // No default constructor
    }

    public BaseValveFormatParser(File toParse) throws IOException, InvalidValveFileException {
        is = new FileInputStream(toParse);
        readAndCheckType();
        parseFields();
        is.close();
    }

    protected abstract void readAndCheckType() throws IOException, InvalidValveFileException;

    protected abstract void parseFields() throws IOException;

    protected String readString(int length) throws IOException {
        int index = 0;
        int toRead = cursor + length;
        int realLength = 0;
        char[] readData = new char[length];
        while (cursor < toRead) {
            int content = is.read();
            if (content != 0) {
                // Until we reached
                realLength++;
            }
            readData[index] = ((char) content);
            cursor++;
            index++;

        }
        return new String(readData, 0, realLength);
    }

    protected byte[] readBytes(int length) throws IOException {
        int index = 0;
        int toRead = cursor + length;
        byte[] readData = new byte[length];
        while (cursor < toRead) {
            readData[index] = (byte) is.read();
            cursor++;
            index++;
        }
        return readData;
    }

    protected int readInt(int length) throws IOException {
        byte[] readData = readBytes(length);
        return ByteBuffer.allocate(length).put(readData).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
    }

    protected float readFloat(int length) throws IOException {
        byte[] readData = readBytes(length);
        return ByteBuffer.allocate(length).put(readData).order(ByteOrder.LITTLE_ENDIAN).getFloat(0);
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    protected static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
