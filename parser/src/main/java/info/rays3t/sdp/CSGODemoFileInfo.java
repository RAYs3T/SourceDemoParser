package info.rays3t.sdp;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RAYs3T-BWS on 12.04.2017.
 */
public class CSGODemoFileInfo extends BaseDemoFileInfo {

    public static final Logger log = LogManager.getLogger(CSGODemoFileInfo.class);
    /*
        Match 1
        Full match	0-60	`auto0-20170305-011810-2076974519-aim_deagle7k_go-WARTUNG.dem`
        Group 1.	4-5	`0`
        Group 2.	6-21	`20170305-011810`
        Group 3.	6-10	`2017`
        Group 4.	10-12	`03`
        Group 5.	12-14	`05`
        Group 6.	15-17	`01`
        Group 7.	17-19	`18`
        Group 8.	19-21	`10`
        Group 9.	22-32	`2076974519`
        Group 10.	33-48	`aim_deagle7k_go`
        Group 11.	49-56	`WARTUNG`
     */
    // TODO If the server name contains any "-" characters the regex will "break".
    // Since this part of the information is not that important, we could ignore this.
    public static final Pattern PATTERN = Pattern.compile("auto(\\d{1})-((\\d{4})(\\d{2})(\\d{2})-(\\d{2})(\\d{2})(\\d{2}))-(\\d+)-(.*)-(.*).dem");

    @Override
    public Pattern getNameParsePattern() {
        return PATTERN;
    }

    /**
     * Parses the given filename
     *
     * @param fileName the demo filename
     * @return true, if the parse succeeded, false if not
     */
    @Override
    public boolean parseFromFilename(String fileName) {
        Matcher matcher = getNameParsePattern().matcher(fileName);
        if (!matcher.find()) {
            log.debug("Tried to parse a file (" + fileName + ") that does not match my regex (" + getNameParsePattern() + ")");
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
        try {
            setDate(sdf.parse(matcher.group(2)));
        } catch (ParseException e) {
            setDate(null);
            log.warn("Cannot parse demo date: " + e.getMessage(), e);
            return false;
        }
        setMapName(matcher.group(10));
        setOriginalFileName(fileName);
        return true;
    }
}
