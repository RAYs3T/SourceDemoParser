package info.rays3t.sdp;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 */
public abstract class BaseDemoFileInfo {


    private String originalFileName;

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public static final String FILE_EXTENSION = ".dem";
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private String mapName;

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public String getFileExtension(){
        return FILE_EXTENSION;
    }

    public abstract Pattern getNameParsePattern();

    public abstract boolean parseFromFilename(String fileName);

}
