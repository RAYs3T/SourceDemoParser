package info.rays3t.sdp;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 */
public class CSSDemoFileInfo extends BaseDemoFileInfo {
    public static final Logger log = LogManager.getLogger(CSSDemoFileInfo.class);
    public static final Pattern PATTERN = Pattern.compile("auto\\-(([\\d]{4})([\\d]{2})([\\d]{2})-([\\d]{2})([\\d]{2}))\\-(.*)\\.dem");

    @Override
    public Pattern getNameParsePattern() {
        return PATTERN;
    }

    /**
     * Parses the given filename
     * @param fileName the demo filename
     * @return true, if the parse succeeded, false if not
     */
    @Override
    public boolean parseFromFilename(String fileName) {
        Matcher matcher = getNameParsePattern().matcher(fileName);
        if (!matcher.find()) {
            log.trace("Tried to parse a file (" + fileName + ") that does not match my regex (" + getNameParsePattern() + ")");
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
        try {
            setDate(sdf.parse(matcher.group(1)));
        } catch (ParseException e) {
            setDate(null);
            log.warn("Cannot parse demo date: " + e.getMessage(), e);
            return false;
        }
        setMapName(matcher.group(7));
        setOriginalFileName(fileName);
        return true;
    }
}
