package info.rays3t.sdp;

import info.rays3t.sdp.parser.DemoInfo;

import java.io.File;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 */
public class Demo {
    private DemoInfo info;

    public DemoInfo getInfo() {
        return info;
    }

    public void setInfo(DemoInfo info) {
        this.info = info;
    }

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
