package info.rays3t.sdp;



import info.rays3t.sdp.parser.DemoInfo;
import info.rays3t.sdp.parser.DemoParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by RAYs3T-BWS on 10.04.2017.
 */
public class DemoParserTest {

    protected static final String cssDemoFileName = "/auto-20170410-1514-jb_italia_final.dem";
    protected static final String csgoDemoFileName = "/auto0-20170409-163908-1700419995-mg_lego_multigames_go_v3_2-_Push_The_Limits__Minigame__Bhop_FDL_VIP_ptl-clan.de_.dem";

    @Test
    public void parseCSSDemo() {
        CSSDemoFileInfo demo = new CSSDemoFileInfo();
        Assert.assertTrue(demo.parseFromFilename(cssDemoFileName));
        Assert.assertEquals(demo.getMapName(), "jb_italia_final");
        Date date = demo.getDate();
        Assert.assertNotNull(date);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        Assert.assertEquals(calendar.get(Calendar.YEAR), 2017);
        Assert.assertEquals(calendar.get(Calendar.MONTH) + 1, 4);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 10);
        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 15);
        Assert.assertEquals(calendar.get(Calendar.MINUTE), 14);
        Assert.assertEquals(calendar.get(Calendar.SECOND), 0);
        Assert.assertEquals(demo.getOriginalFileName(), cssDemoFileName);
    }


    @Test
    public void parseCSGODemo() {
        CSGODemoFileInfo demo = new CSGODemoFileInfo();
        Assert.assertTrue(demo.parseFromFilename(csgoDemoFileName));
        //Assert.assertEquals(demo.getMapName(), "mg_lego_multigames_go_v3_2"); // Stinde the regex cannot deal with "-" in the server name
        Date date = demo.getDate();
        Assert.assertNotNull(date);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        Assert.assertEquals(calendar.get(Calendar.YEAR), 2017);
        Assert.assertEquals(calendar.get(Calendar.MONTH) + 1, 4);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 9);
        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 16);
        Assert.assertEquals(calendar.get(Calendar.MINUTE), 39);
        Assert.assertEquals(calendar.get(Calendar.SECOND), 8);
        Assert.assertEquals(demo.getOriginalFileName(), csgoDemoFileName);
    }

    private File createFileFromResource(String resourcePath) throws IOException {
        InputStream in = DemoParserTest.class.getResourceAsStream(resourcePath);
        Assert.assertNotNull(in, "Unable to load resource: " + resourcePath);
        File tempFile = File.createTempFile("demo_parser_", ".dem.tmp");
        OutputStream outputStream =
                new FileOutputStream(tempFile);

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = in.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        outputStream.close();
        in.close();
        return tempFile;
    }

    @Test
    public void testParserCSS() throws IOException, InvalidValveFileException {
        File demoFile = createFileFromResource(cssDemoFileName);
        Assert.assertNotNull(demoFile);

        DemoParser parser = new DemoParser(demoFile);
        DemoInfo demo = parser.getDemo();
        Assert.assertEquals(demo.getDemoProtocol(), 3);
        Assert.assertEquals(demo.getNetProtocol(), 24);
        Assert.assertEquals(demo.getHostName(), "[GER]*Push The Limits-Jail*[!w!e!v|FDL|VIP|100Tick|ptl-clan.de]");
        Assert.assertEquals(demo.getClientName(), "SourceTV Demo");
        Assert.assertEquals(demo.getMapName(), "jb_italia_final");
        Assert.assertEquals(demo.getGameDir(), "cstrike");
        Assert.assertEquals(demo.getTime(), 396.10498046875f);
        Assert.assertEquals(demo.getTicks(), 26407);
        Assert.assertEquals(demo.getFrames(), 6599);
        Assert.assertEquals(demo.getTickrate(), 66);
        Assert.assertEquals(demo.getType(), 1);
    }

    @Test
    public void testParserCSGO() throws IOException, InvalidValveFileException {

        File demoFile = createFileFromResource(csgoDemoFileName);
        Assert.assertNotNull(demoFile);

        DemoParser parser = new DemoParser(demoFile);
        DemoInfo demo = parser.getDemo();
        Assert.assertEquals(demo.getDemoProtocol(), 4);
        Assert.assertEquals(demo.getNetProtocol(), 13573);
        Assert.assertEquals(demo.getHostName(), "*Push The Limits* Minigame [Bhop|FDL|VIP|ptl-clan.de]");
        Assert.assertEquals(demo.getClientName(), "GOTV Demo");
        Assert.assertEquals(demo.getMapName(), "mg_lego_multigames_go_v3_2");
        Assert.assertEquals(demo.getGameDir(), "csgo");
        Assert.assertEquals(demo.getTime(), 42.84375f);
        Assert.assertEquals(demo.getTicks(), 2742);
        Assert.assertEquals(demo.getFrames(), 1371);
        Assert.assertEquals(demo.getTickrate(), 64);
        Assert.assertEquals(demo.getType(), 1);
    }

    @Test(expectedExceptions = InvalidValveFileException.class)
    public void testParserWithInvalidFile() throws InvalidValveFileException, IOException {
        File demoFile = createFileFromResource("/invalid_file.dem");
        Assert.assertNotNull(demoFile);
        DemoParser parser = new DemoParser(demoFile);
    }
}
