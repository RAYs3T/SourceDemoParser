package info.rays3t.sdp;

/**
 * Created by RAYs3T-BWS on 13.04.2017.
 */
public class InvalidValveFileException extends Exception {
    public InvalidValveFileException(String message) {
        super(message);
    }
}
